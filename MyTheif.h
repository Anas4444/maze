// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyTheif.generated.h"

class USpringArmComponent;
class UCameraComponent;

UENUM(BlueprintType)
enum class EMovementStat : uint8 {
	MS_Normal UMETA(DisplayName = "Normal"),
	MS_Sprinting UMETA(DisplayName = "Sprinting")
};

UCLASS()
class DETECTIVEVSPRISONER_API AMyTheif : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyTheif();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* FollowCamera;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		EMovementStat MovementState;
	
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* myGear;

	UPROPERTY(BlueprintReadOnly)
		float MovementSpeed;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float WalkingSpeed = 600.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float RunningSpeed = 1000.f;
	
	UPROPERTY(BlueprintReadOnly)
		bool bIsAlive = true;

	UPROPERTY(BlueprintReadOnly)
		bool bIsAttacking = false;
	
	UPROPERTY(BlueprintReadOnly)
		bool bInAir;

	//Called for forwards/backward input

	void MoveForward(float InputAxis);

	//called for left/right side input

	void MoveRight(float InputAxis);

	virtual void Jump() override;

	//Sets Character Movement Speed to Sprint values.
    void BeginSprint();

    //Sets Character Movement Speed back to default speed values.
    void EndSprint();

	void SetMovementSpeedAndAir();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};

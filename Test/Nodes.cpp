#include "Nodes.h"

template<class T, class V>
Nodes<T, V>::Nodes(const Nodes<T, V>& node)
{
    this->state = node.state;
    this->action = node.action;
}

template<class T, class V>
Nodes<T, V>::Nodes(T s, V a)
{
    this->state = s;
    this->action = a;
}

template<class T, class V>
Nodes<T, V>::Nodes(T s)
{
    this->state = s;
    this->action = "";
}

template<class T, class V>
void Nodes<T, V>::setNode(T s, V a)
{
    this->state = s;
    this->action = a;
}

template<class T, class V>
void Nodes<T, V>::setNode(T s)
{
    this->state = s;
    this->action = "";
}

template<class T, class V>
void Nodes<T, V>::showState()
{
    std::cout << "[(" << std::get<0>(state) << ", " << std::get<1>(state) << "), ";
}

template<class T, class V>
void Nodes<T, V>::showAction()
{
    std::cout << action << "]" << std::endl;
}

#pragma once

template <class T, class V>
struct Nodes
{
    T state;
    V action;

    Nodes(const Nodes<T, V>& node);

    Nodes(T s, V a);

    Nodes(T s);

    void setNode(T s, V a);

    void setNode(T s);

    void showState();

    void showAction();
};
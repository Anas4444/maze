// Copyright Epic Games, Inc. All Rights Reserved.

#include "DetectiveVsPrisoner.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DetectiveVsPrisoner, "DetectiveVsPrisoner" );

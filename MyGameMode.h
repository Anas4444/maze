// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "AI/AI.h"
#include "MyGameMode.generated.h"



/**
 * 
 */

class AActor;

UCLASS()
class DETECTIVEVSPRISONER_API AMyGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	AMyGameMode();
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	//TArray<TSubclassOf<AActor>> roads;

	void initBoard();
	//void spawnRout();
	
	
};

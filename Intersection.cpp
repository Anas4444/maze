// Fill out your copyright notice in the Description page of Project Settings.


#include "Intersection.h"
#include "TrafficLight.h"
#include <random>

// Sets default values
AIntersection::AIntersection()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AIntersection::BeginPlay()
{
	Super::BeginPlay();
	createTrafficLight();
	
}

// Called every frame
void AIntersection::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIntersection::createTrafficLight()
{
	FVector v1 = FVector(GetActorLocation().X+460.f, GetActorLocation().Y+460.f, GetActorLocation().Z);
	FRotator r1 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw, GetActorRotation().Roll);

	FVector v2 = FVector(GetActorLocation().X-460.f, GetActorLocation().Y-460.f, GetActorLocation().Z);
	FRotator r2 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw+180.f, GetActorRotation().Roll);

	FVector v3 = FVector(GetActorLocation().X+460.f, GetActorLocation().Y-460.f, GetActorLocation().Z);
	FRotator r3 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw-90.f, GetActorRotation().Roll);

	FVector v4 = FVector(GetActorLocation().X-460.f, GetActorLocation().Y+460.f, GetActorLocation().Z);
	FRotator r4 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw+90.f, GetActorRotation().Roll);

	tl[0] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v1, &r1));
	tl[1] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v2, &r2));
	tl[2] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v3, &r3));
	tl[3] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v4, &r4));

	constexpr int DOUBLE_MIN = 0;
	constexpr int DOUBLE_MAX = 1;
	std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_real_distribution<double> distr(DOUBLE_MIN, DOUBLE_MAX);

	double r[4];
	int s[4] = {0, 0, 0, 0};
	for (int i=0; i<4; i++) {
		r[i] = distr(eng);
	}
	double temp;
	for (int i=0; i<4; i++) {
		temp = r[i];
		for (int j = 0; j<4; j++) {
			if (temp >= r[j]) s[i]++;
		}
	}
	for (int i=0; i<4; i++) {
		tl[i]->intersection = 0;
		tl[i]->order = s[i];
	}
}
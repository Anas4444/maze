// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Intersection.generated.h"

class ATrafficLight;

UCLASS()
class DETECTIVEVSPRISONER_API AIntersection : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIntersection();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<ATrafficLight> TrafficLight;
	
	ATrafficLight* tl[4];

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void createTrafficLight();


};

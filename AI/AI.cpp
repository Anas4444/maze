#include "AI.h"

inline AI::AI() {
    AI::bots.push_back(this);
}

inline AI::AI(const Coordinate<int>& pos) {
    AI::bots.push_back(this);
    this->position = pos;
    this->destination = Coordinate<int>(8, 3);
}

inline AI::AI(const Coordinate<int>& pos, const Coordinate<int>& dest) {
    AI::bots.push_back(this);
    this->destination = dest;
    this->position = pos;
}

inline AI::AI(const Coordinate<int>& pos, const Coordinate<int>& dest, int nType) {
    AI::bots.push_back(this);
    this->destination = dest;
    this->position = pos;
    this->neighbourType = nType;
}

inline AI::AI(const Coordinate<int>& pos, const Coordinate<int>& dest, int nType, int weight) {
    AI::bots.push_back(this);
    this->destination = dest;
    this->position = pos;
    this->neighbourType = nType;
    this->weight = weight;
}

inline AI::~AI() {
    int n = AI::bots.size();
    /*for (int i=0; i<n-1; i++) {
        std::cout << AI::bots[i] << " | ";
    }
    std::cout << AI::bots[n-1] << "\n";*/
    for (int i=0; i<n; i++) {
        if (AI::bots[i]==this) {
            AI::bots.erase(AI::bots.begin()+i);
            break;
        }
    }
    this->clearPathFinding();
    this->clearSearch();

}

inline void AI::setPosition(const Coordinate<int>& pos)
{
    this->position = pos;
}

inline void AI::setDestination(const Coordinate<int>& dest)
{
    this->destination = dest;
}

inline void AI::setWeight(int weigh)
{
    this->weight = weigh;
}

inline void AI::clearSearch() {
    int n = this->allSearch.size();
    if (n==0) return;
    this->allSearch.clear();
    this->allSearch.shrink_to_fit();
}

inline void AI::clearPathFinding() {
    this->eraseExplored();
    this->eraseFrontier();
    this->eraseAllPaths();
    this->weight = 0;
}

inline void AI::eraseExplored() {
    //for (int i=0; i<this->explored.size(); i++) delete this->explored[i];
    this->explored.clear();
    this->explored.shrink_to_fit();
}

inline void AI::eraseBoard() {
    for (int i=0; i<AI::dimensions.y; i++) delete[] AI::board[i];
    delete[] AI::board;
}

inline void AI::eraseFrontier() {
    delete this->frontier;
}

inline void AI::eraseAllPaths() {
    int n = this->allPaths.size();
    if (n==0) return;
    for (int i=0; i<n; i++) {
        int m = this->allPaths[i].size();
        if (m==0) continue;
        //delete this->allPaths[i][m-1];
        this->allPaths[i].clear();
        this->allPaths[i].shrink_to_fit();
    }
    this->allPaths.clear();
    this->allPaths.shrink_to_fit();
}

inline std::vector<std::string> AI::fillText(const std::string& path) {
    std::string textt;
    std::ifstream file(path);
    std::vector<std::string> allText;
    while(std::getline(file, textt)) {
        allText.push_back(textt);
    }
    file.close();
    return allText;
}

inline std::vector<FString> AI::fillTextUE(const FString& path)
{
    std::vector<FString> content;
    IPlatformFile& FileManager = FPlatformFileManager::Get().GetPlatformFile();
    FString file = FPaths::ProjectConfigDir();
    file.Append("maze1.txt");
    //file = FString::Printf(TEXT("%s%s"), *file, &path);

    FString FileContent;
    if (FileManager.FileExists(*file)) {
        FFileHelper::LoadFileToString(FileContent, *file); 
        FString line1;
        while (FileContent.Split(TEXT("\n"), &line1, &FileContent)) { content.push_back(line1); }
        content.push_back(FileContent);
    }
    return content;
}

inline Coordinate<int> AI::fillDimensions() {
    return Coordinate<int>(AI::text[0].length(), AI::text.size());
}

inline Coordinate<int> AI::fillDimensionsUE() {
    return Coordinate<int>(AI::textUE[0].Len()-1, AI::textUE.size());
}

inline int** AI::fillBoard() {
    int** block = new int*[dimensions.y];
    for (int i=0; i<AI::dimensions.y; i++) block[i] = new int[AI::dimensions.x];
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            if (AI::text[i][j]!='&') block[i][j] = 0;
            else block[i][j] = -1;
        }
    }
    return block;
}

inline int** AI::fillBoardUE()
{
    int** block = new int*[dimensions.y];
    for (int i=0; i<AI::dimensions.y; i++) block[i] = new int[AI::dimensions.x];
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            FString currChar = AI::textUE[i].Mid(j, 1);
            if (currChar.Equals(TEXT("&"))) block[i][j] = -1;
            else block[i][j] = 0;
        }
    }
    return block;
}

inline void AI::print() {
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            if (AI::board[i][j]==-1) std::cout << '&';
            else if (this->position==Coordinate<int>(j, i)) std::cout << 'P'; 
            else if (this->destination==Coordinate<int>(j, i)) std::cout << 'L';
            else if (AI::board[i][j]>=0) std::cout << ' ';
            else if (AI::board[i][j]==-2) std::cout << '*';
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

inline void AI::showAll(int w=0) {
    if (w==0) w = this->weight;
    for (int i=0; i<w; i++) {
        this->drawPath(i+1);
        this->print();
        this->zeroPath();
    }
}

inline void AI::explore() {
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            if (AI::board[i][j]==-1) std::cout << '&';
            else std::cout << AI::board[i][j];
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

inline void AI::zeroPath() {
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            if (AI::board[i][j]==-2) AI::board[i][j]=0;
        }
    }
}

inline void AI::zero() {
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            if (AI::board[i][j]>0 || AI::board[i][j]==-2) AI::board[i][j]=0;
        }
    }
}

inline void AI::zero(Node<Coordinate<int>>* node) {
    Node<Coordinate<int>>* p = node;
    while (p != nullptr) {
        Coordinate<int> pos = p->getData();
        AI::board[pos.y][pos.x] = 0;
        p = p->parent;
    }
}

inline int AI::roadType(const Coordinate<int>& road)
{
    if (AI::board[road.y][road.x]==-1) return -1;
    std::vector<Coordinate<int>> neigh = AI::neighbours4(road);  
    int n = neigh.size();
    if (n==4) return 11;
    if (n==0) return 0;
    if (n==3) {
        if (road.x==neigh[0].x && road.x==neigh[1].x || road.x!=neigh[0].x && road.x!=neigh[1].x) {
            if (road.x==neigh[2].x) {
                if (road.y<neigh[2].y) return 7;
                else return 9;
            }
            else {
                if (road.x<neigh[2].x) return 8;
                else return 10;
            }
        }
        else if (road.x==neigh[0].x && road.x!=neigh[1].x && road.x!=neigh[2].x || road.x!=neigh[0].x && road.x==neigh[1].x && road.x==neigh[2].x) {
            if (road.x==neigh[0].x) {
                if (road.y<neigh[0].y) return 7;
                else return 9;
            }
            else {
                if (road.x<neigh[0].x) return 8;
                else return 10;
            }
        }
        else {
            if (road.x==neigh[1].x) {
                if (road.y<neigh[1].y) return 7;
                else return 9;
            }
            else {
                if (road.x<neigh[1].x) return 8;
                else return 10;
            }
        }
    }
    if (road.x==neigh[1].x && road.y==neigh[0].y) {
        if (road.x<neigh[0].x && road.y<neigh[1].y) return 3;
        if (road.x<neigh[0].x && road.y>neigh[1].y) return 4;
        if (road.x>neigh[0].x && road.y>neigh[1].y) return 5;
        if (road.x>neigh[0].x && road.y<neigh[1].y) return 6;
    }
    if (road.x==neigh[0].x && road.y==neigh[1].y) {
        if (road.x<neigh[1].x && road.y<neigh[0].y) return 3;
        if (road.x<neigh[1].x && road.y>neigh[0].y) return 4;
        if (road.x>neigh[1].x && road.y>neigh[0].y) return 5;
        if (road.x>neigh[1].x && road.y<neigh[0].y) return 6;
    }
    if (road.x==neigh[0].x) return 1;
    else return 2;
}

inline void AI::drawPath(const int& alpha=1) {
    if (this->allPaths.size()>alpha-1) {
        for (int i=0; i<this->allPaths[alpha-1].size(); i++) {
            Coordinate<int> pos = this->allPaths[alpha-1][i]->getData();
            AI::board[pos.y][pos.x]=-2;
        }
    }
}

inline void AI::draw() {
    for (int i=0; i<this->allPaths.size(); i++) {
        for (int j=0; j<this->allPaths[i].size(); j++) {
            Coordinate<int> pos = this->allPaths[i][j]->getData();
            AI::board[pos.y][pos.x]=-2;
        }
    }
}

inline void AI::draw(Node<Coordinate<int>>* node) {
    Node<Coordinate<int>>* p = node;
    while (p != nullptr) {
        Coordinate<int> pos = p->getData();
        AI::board[pos.y][pos.x] = -2;
        p = p->parent;
    }
}

inline bool AI::inExplored(const Coordinate<int>& pair) {
    for (int i=0; i<this->explored.size(); i++) {
        Coordinate<int> pos = this->explored[i]->getData();
        if (pos==pair) return true;
    }
    return false;
    //return AI::board[pair.x][pair.y]>0;
}

inline bool AI::isExplored(const Coordinate<int>& data, Node<Coordinate<int>>* parent) {
    for (int i=0; i<this->explored.size(); i++) {
        if (this->explored[i]->parent && parent) 
            if (this->explored[i]->parent->getData()==data && this->explored[i]->equals(parent)) return true;
    }
    return false;
}

inline bool AI::isDuplicate(Node<Coordinate<int>>* node) {
    Node<Coordinate<int>>* p = node;
    std::vector<Node<Coordinate<int>>*> path;
    while (p != nullptr) {
        Coordinate<int> pos = p->getData();
        path.push_back(p);
        p = p->parent;
    }
    int n = path.size();
    for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
            if (path[i]->getData()==path[j]->getData() && i!=j) return true;
        }
    }
    return false;
}

inline bool AI::allChecked() {
    for (int i=0; i<AI::dimensions.y; i++) {
        for (int j=0; j<AI::dimensions.x; j++) {
            if (AI::board[i][j]>0) {
                std::vector<Coordinate<int>> neighbors = this->getNeighbours(Coordinate<int>(j, i));
                for (int k=0; k<neighbors.size(); k++) {
                    Coordinate<int> pos = neighbors[k];
                    if (AI::board[pos.y][pos.x]==0) return false;
                }
            }
        }
    }
    return true;
}

inline Coordinate<int> AI::bestPosition8(const Coordinate<int>& loc) {
    Coordinate<int> nghb[8] {Coordinate<int>(loc.x+1, loc.y), Coordinate<int>(loc.x-1, loc.y),
                             Coordinate<int>(loc.x, loc.y+1), Coordinate<int>(loc.x, loc.y-1),
                             Coordinate<int>(loc.x+1, loc.y+1), Coordinate<int>(loc.x+1, loc.y-1),
                             Coordinate<int>(loc.x-1, loc.y+1), Coordinate<int>(loc.x-1, loc.y-1)};
    Coordinate<int> pos;
    int i=0;
    for (; i<8; i++) {
        if (nghb[i].x < AI::dimensions.x && nghb[i].y < AI::dimensions.y && nghb[i].x >= 0 && nghb[i].y >= 0 && AI::board[nghb[i].y][nghb[i].x]!=-1) {
            pos = nghb[i];
            break;
        }
    }
    for (; i<8; i++) {
        bool notBlockandInBoard = nghb[i].x < AI::dimensions.x && nghb[i].y < AI::dimensions.y && nghb[i].x >= 0 && nghb[i].y >= 0 && AI::board[nghb[i].y][nghb[i].x]!=-1;
        if (!notBlockandInBoard) continue;
        if (AI::board[pos.y][pos.x] > AI::board[nghb[i].y][nghb[i].x]) pos = nghb[i];
    }
    return pos;
}

inline Coordinate<int> AI::bestPosition4(const Coordinate<int>& loc) {
    Coordinate<int> nghb[4] {Coordinate<int>(loc.x+1, loc.y), Coordinate<int>(loc.x-1, loc.y),
                             Coordinate<int>(loc.x, loc.y+1), Coordinate<int>(loc.x, loc.y-1)};
    Coordinate<int> pos;
    int i=0;
    for (; i<4; i++) {
        if (nghb[i].x < AI::dimensions.x && nghb[i].y < AI::dimensions.y && nghb[i].x >= 0 && nghb[i].y >= 0 && AI::board[nghb[i].y][nghb[i].x]!=-1) {
            pos = nghb[i];
            break;
        }
    }
    for (; i<4; i++) {
        bool notBlockandInBoard = nghb[i].x < AI::dimensions.x && nghb[i].y < AI::dimensions.y && nghb[i].x >= 0 && nghb[i].y >= 0 && AI::board[nghb[i].y][nghb[i].x]!=-1;
        if (!notBlockandInBoard) continue;
        if (AI::board[pos.y][pos.x] > AI::board[nghb[i].y][nghb[i].x]) pos = nghb[i];
    }
    return pos;
}

inline std::vector<Coordinate<int>> AI::neighbours8(const Coordinate<int>& loc) {
    Coordinate<int> nghb[8] {Coordinate<int>(loc.x+1, loc.y), Coordinate<int>(loc.x-1, loc.y),
                             Coordinate<int>(loc.x, loc.y+1), Coordinate<int>(loc.x, loc.y-1),
                             Coordinate<int>(loc.x+1, loc.y+1), Coordinate<int>(loc.x+1, loc.y-1),
                             Coordinate<int>(loc.x-1, loc.y+1), Coordinate<int>(loc.x-1, loc.y-1)}; 
    std::vector<Coordinate<int>> result;
    for (int i=0; i<8; i++) {
        if (nghb[i].x < AI::dimensions.x && nghb[i].y < AI::dimensions.y && nghb[i].x >= 0 && nghb[i].y >= 0 && AI::board[nghb[i].y][nghb[i].x]!=-1)
            result.push_back(nghb[i]);
    }
    return result;
}

inline std::vector<Coordinate<int>> AI::neighbours4(const Coordinate<int>& loc) {
    Coordinate<int> nghb[4] {Coordinate<int>(loc.x+1, loc.y), Coordinate<int>(loc.x-1, loc.y),
                             Coordinate<int>(loc.x, loc.y+1), Coordinate<int>(loc.x, loc.y-1)}; 
    std::vector<Coordinate<int>> result;
    for (int i=0; i<4; i++) {
        if (nghb[i].x < AI::dimensions.x && nghb[i].y < AI::dimensions.y && nghb[i].x >= 0 && nghb[i].y >= 0 && AI::board[nghb[i].y][nghb[i].x]!=-1)
            result.push_back(nghb[i]);
    }
    return result;
}

inline bool AI::hasPath() {
    this->clearPathFinding();
    this->initFrontier();
    std::vector<Node<Coordinate<int>>*> path;
    //this->frontier->printHBoard();
    Node<Coordinate<int>>* node = new Node<Coordinate<int>>(this->position);
    this->frontier->add(node);
    while(node->getData()!=destination && !this->frontier->empty()) {
        //this->frontier->printOne();
        node = this->frontier->remove();
        this->explored.push_back(node);
        //std::cout << "Best One : ";
        //node->printOne();
        //std::cout << " | distance : " << this->frontier->distance(node->getData()) << "\n\n";
        Coordinate<int> pos = node->getData();
        AI::board[pos.y][pos.x]++;
        //this->explore();
        std::vector<Coordinate<int>> neighb = this->getNeighbours(node->getData());
        for (int i=0; i<neighb.size(); i++) {
            if (!inExplored(neighb[i]) && !this->frontier->inFrontier(neighb[i])) {
                Node<Coordinate<int>>* newNode = new Node<Coordinate<int>>(neighb[i], node);
                this->frontier->add(newNode);
            } 
        }
    }
    if (node->getData()!=destination) return false;
    return true;
}

inline void AI::shortestPath() {
    this->clearPathFinding();
    this->weight = 1;
    this->initFrontier();
    std::vector<Node<Coordinate<int>>*> path;
    //this->frontier->printHBoard();
    Node<Coordinate<int>>* node = new Node<Coordinate<int>>(this->position);
    this->frontier->add(node);
    while(node->getData()!=destination && !this->frontier->empty()) {
        //this->frontier->printOne();
        node = this->frontier->remove();
        //std::cout << "Best One : ";
        //node->printOne();
        //std::cout << " | distance : " << frontier->distance(node->getData()) << "\n\n";
        Coordinate<int> pos = node->getData();
        AI::board[pos.y][pos.x]++;
        //this->explore();
        std::vector<Coordinate<int>> neighb = this->getNeighbours(node->getData());
        for (int i=0; i<neighb.size(); i++) {
            if (!inExplored(neighb[i]) && !this->frontier->inFrontier(neighb[i])) {
                Node<Coordinate<int>>* newNode = new Node<Coordinate<int>>(neighb[i], node);
                this->frontier->add(newNode);
            }

        }
    }
    Node<Coordinate<int>>* p = node;
    while (p != nullptr) {
        path.push_back(p);
        p = p->parent;
    }
    std::reverse(path.begin(), path.end());
    this->allPaths.push_back(path);
}

inline void AI::alphaShortestPath(int w = 0) {
    if (!hasPath()) {
        this->weight = 0;
        return;
    }
    this->clearPathFinding();
    this->weight = w;
    std::vector<Node<Coordinate<int>>*> allNodes;
    this->initFrontier();
    //this->frontier->printHBoard();
    Node<Coordinate<int>>* node = new Node<Coordinate<int>>(this->position);
    this->frontier->add(node);
    int k=0;
    int d=0;
    while(!this->frontier->empty() && (k<w || w<=0) && d<10) {
        //frontier->printOne();
        node = this->frontier->remove();
        //std::cout << "Best One : ";
        //node->printOne();
        //std::cout << " | distance : " << frontier->distance(node->getData()) << "\n\n";
        while (node->getData()==destination && (k<w || w<=0)) {
            if (!isDuplicate(node)) {
                //node->print();
                std::vector<Node<Coordinate<int>>*> path;
                Node<Coordinate<int>>* p = node;
                while (p != nullptr) {
                    path.push_back(p);
                    p = p->parent;
                }
                
                std::reverse(path.begin(), path.end());
                this->allPaths.push_back(path);
                d = 0;
                k++;

                Coordinate<int> pos = node->getData();
                AI::board[pos.y][pos.x]++;
                this->explored.push_back(node);
            }
            d++;
            //std::cout << k <<" Explored\n\n";
            //this->frontier->printOne();
            node = this->frontier->remove();
            //std::cout << "Best One : ";
            //node->printOne();
            //std::cout << " | distance : " << this->frontier->distance(node->getData()) << "\n\n";
        }
        Coordinate<int> pos = node->getData();
        AI::board[pos.y][pos.x]++;
        this->explored.push_back(node);
        //AI::explore();
        std::vector<Coordinate<int>> neighb = this->getNeighbours(node->getData());
        for (int i=0; i<neighb.size(); i++) {
            if (!isExplored(neighb[i], node)) {
                Node<Coordinate<int>>* newNode = new Node<Coordinate<int>>(neighb[i], node);
                this->frontier->add(newNode);
            }
        }
    }
    if (this->weight>this->allPaths.size() || this->weight<=0)
        this->weight = this->allPaths.size();
}

inline void AI::search() {
    clearSearch();
    this->allSearch.push_back(position);
    AI::board[position.y][position.x]++;
    //AI::explore();
    while (true) {
        int n = this->allSearch.size();
        this->position = this->allSearch[n-1];
        Coordinate<int> newPos = this->getBestPosition(position);
        if (AI::board[newPos.y][newPos.x]>0 && AI::board[position.y][position.x]==1) {
            if (this->allChecked()) break;
        }
        AI::board[newPos.y][newPos.x]++;
        //this->explore();
        this->position = newPos;
        this->allSearch.push_back(newPos);
    }
}

inline void AI::multiSearch() {
    for (auto bot : AI::bots) {
        bot->allSearch.push_back(bot->position);
        AI::board[bot->position.y][bot->position.x]++;
        //std::cout << "bot : " << bot << std::endl;
        //AI::explore();
    }
    int b = true;
    while (b) {
        for (auto bot : AI::bots) {
            int n = bot->allSearch.size();
            bot->position = bot->allSearch[n-1];
            Coordinate<int> newPos = bot->getBestPosition(bot->position);
            if (AI::board[newPos.y][newPos.x]>0 && AI::board[bot->position.y][bot->position.x]>=1) {
                if (bot->allChecked()) {
                    b = false;
                    break;
                }
            }
            AI::board[newPos.y][newPos.x]++;
            //std::cout << "bot : " << bot << std::endl;
            //AI::explore();
            bot->position = newPos;
            bot->allSearch.push_back(newPos);
        }
    }
}

inline void AI::initFrontier()
{
    switch(AI::frontierType) {
        case 0:
            this->frontier = new StackFrontier<int>();
            break;
        case 1:
            this->frontier = new QueueFrontier<int>();
            break;
        case 2:
            this->frontier = new GreedyFrontier<int>(&this->destination, AI::dimensions);
            break;
        default:
            this->frontier = new AStarFrontier<int>(&this->destination, AI::dimensions);
    }
}

inline std::vector<Coordinate<int>> AI::getNeighbours(const Coordinate<int>& location)
{
    if (this->neighbourType==0) return AI::neighbours4(location);
    return AI::neighbours8(location);
}

inline Coordinate<int> AI::getBestPosition(const Coordinate<int>& location)
{
    if (this->neighbourType==0) return AI::bestPosition4(location);
    return AI::bestPosition8(location);
}

inline void AI::setNeighbourType(int nType)
{
    this->neighbourType = nType;
}
#pragma once
#include <fstream>
#include <algorithm>
#include <string>
#include "AStarFrontier.cpp"

class AI {
    public:
        static std::vector<AI*> bots;
        static std::vector<std::string> text;
        static std::vector<FString> textUE;
        static int** board;
        static Coordinate<int> dimensions;
        static int frontierType;

    private:
        int weight = 0;
        int neighbourType = 0;
        
        Coordinate<int> destination;        
        Coordinate<int> position;

        StackFrontier<int>* frontier;

        std::vector<Node<Coordinate<int>>*> explored;
        std::vector<std::vector<Node<Coordinate<int>>*>> allPaths;
        std::vector<Coordinate<int>> allSearch;
        
    public:
    //Constructors & Destructors
        AI();
        AI(const Coordinate<int>& pos);
        AI(const Coordinate<int>& pos, const Coordinate<int>& dest);
        AI(const Coordinate<int>& pos, const Coordinate<int>& dest, int neighbourType);
        AI(const Coordinate<int>& pos, const Coordinate<int>& dest, int neighbourType, int weight);
        virtual ~AI();

    //Static functions
        std::vector<std::string> fillText(const std::string& path);
        std::vector<FString> fillTextUE(const FString& path);
        Coordinate<int> fillDimensions();
        Coordinate<int> fillDimensionsUE();
        int** fillBoard();
        int** fillBoardUE();

        void explore();
        void draw(Node<Coordinate<int>>* node);

        void zero();
        void eraseBoard();

        int roadType(const Coordinate<int>& road);

        std::vector<Coordinate<int>> neighbours4(const Coordinate<int>& location);
        Coordinate<int> bestPosition4(const Coordinate<int>& location);

        std::vector<Coordinate<int>> neighbours8(const Coordinate<int>& location);
        Coordinate<int> bestPosition8(const Coordinate<int>& location);

        void multiSearch();

    //Methods
        void initFrontier();

        std::vector<Coordinate<int>> getNeighbours(const Coordinate<int>& location);
        Coordinate<int> getBestPosition(const Coordinate<int>& location);

        void setNeighbourType(int nType);
        void setPosition(const Coordinate<int>& pos);
        void setDestination(const Coordinate<int>& dest);
        void setWeight(int weight);

        void showAll(int weight);
        void print();
    
        void draw();
        void drawPath(const int& i);

        void zeroPath();
        void zero(Node<Coordinate<int>>* node);

        bool isDuplicate(Node<Coordinate<int>>* node);
        bool inExplored(const Coordinate<int>& Coordinate);
        bool isExplored(const Coordinate<int>& data, Node<Coordinate<int>>* parent);
        bool allChecked();
        bool hasPath();

        void shortestPath();
        void alphaShortestPath(int w);
        void search();
        
        void clearPathFinding();
        void clearSearch();

        void eraseAllPaths();
        void eraseExplored();
        void eraseFrontier();
};
/*std::vector<std::string> AI::text = AI::fillText("maze1.txt");
std::vector<AI*> AI::bots;
Coordinate<int> AI::dimensions = AI::fillDimensions();
int** AI::board = AI::fillBoard();
int AI::frontierType = 3;*/
#pragma once
#include <iostream>

template <class S>
class Coordinate {
public:
    S x, y, z;
    
    Coordinate();
    Coordinate(S x, S y);
    Coordinate(S x, S y, S z);
    ~Coordinate();

    void set(int x, int y);
    void set(int x, int y, int z);
    
    int operator==(Coordinate<S> const &m) const;

    int operator!=(Coordinate<S> const &m) const;

    int operator<=(Coordinate<S> const &m) const;

    int operator>=(Coordinate<S> const &m) const;
};

/*template <class S>
std::ostream &operator<<(std::ostream &os, Coordinate<S> const &m) { 
    return os << "(" << m.x << ", " << m.y << ", " << m.z << ")";
}*/
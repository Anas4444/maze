#include "Coordinate.h"

template <class S>
Coordinate<S>::Coordinate() {
        this->x = 0;
        this->y = 0;
        this->z = 0;
}

template <class S>
Coordinate<S>::Coordinate(S x, S y) {
    this->x = x;
    this->y = y;
    this->z = 0;
}

template <class S>
Coordinate<S>::Coordinate(S x, S y, S z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

template <class S>
Coordinate<S>::~Coordinate() {}

template <class S>
void Coordinate<S>::set(int x, int y) {
    this->x = x;
    this->y = y;
}
template <class S>
void Coordinate<S>::set(int x, int y, int z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

template <class S>
int Coordinate<S>::operator==(Coordinate<S> const &m) const { 
    return (x==m.x && y==m.y && z==m.z);
}

template <class S>
int Coordinate<S>::operator!=(Coordinate<S> const &m) const { 
    return (x!=m.x || y!=m.y || z!=m.z);
}

template <class S>
int Coordinate<S>::operator<=(Coordinate<S> const &m) const {
    return (x<=m.x && y<=m.y && z<=m.z);
}

template <class S>
int Coordinate<S>::operator>=(Coordinate<S> const &m) const {
    return (x>=m.x && y>=m.y && z>=m.z);
}

/*template <class S>
std::ostream &operator<<(std::ostream &os, Coordinate<S> const &m) { 
    return os << "(" << m.x << ", " << m.y << ", " << m.z << ")";
}*/
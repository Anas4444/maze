// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTheif.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AMyTheif::AMyTheif()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->TargetArmLength = 600.0f;
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	myGear = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gear"));
	myGear->SetupAttachment(GetMesh(), TEXT("headSocket"));
	
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	// add character movements settings
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 550.0f;
	GetCharacterMovement()->AirControl = 0.2f;

}

// Called when the game starts or when spawned
void AMyTheif::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyTheif::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetMovementSpeedAndAir();

}

// Called to bind functionality to input
void AMyTheif::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMyTheif::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AMyTheif::MoveRight);
    PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAction("Sprint",IE_Pressed,this,&AMyTheif::BeginSprint);
    PlayerInputComponent->BindAction("Sprint",IE_Released,this,&AMyTheif::EndSprint);
	PlayerInputComponent->BindAction("Jump",IE_Pressed,this,&AMyTheif::Jump);
}

void AMyTheif::MoveForward(float AxisValue)
{
	if ((Controller != nullptr) && (AxisValue != 0.0f))
    {
        // Find out which way is forward
        const FRotator Rotation = Controller->GetControlRotation();
        const FRotator YawRotation(0, Rotation.Yaw, 0);

        // Get forward vector
        const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
        AddMovementInput(Direction, AxisValue);
    }
}

void AMyTheif::MoveRight(float AxisValue)
{
	if (!bIsAlive) return;
	if ((Controller != nullptr) && (AxisValue != 0.0f))
    {
        // Find out which way is right
        const FRotator Rotation = Controller->GetControlRotation();
        const FRotator YawRotation(0, Rotation.Yaw, 0);

        // Get right vector 
        const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

        // Add movement in that direction
        AddMovementInput(Direction, AxisValue);
    }
}

void AMyTheif::Jump()
{
	if (!bIsAlive) return;
	if (bIsAttacking) return;
	Super::Jump();
}

void AMyTheif::BeginSprint()
{
	if (!bIsAlive) return;
	if (bIsAttacking) return;
	MovementState = EMovementStat::MS_Sprinting;
	GetCharacterMovement()->MaxWalkSpeed = RunningSpeed;    
}

void AMyTheif::EndSprint()
{
	if (!bIsAlive) return;
	if (bIsAttacking) return;
	MovementState = EMovementStat::MS_Normal;
	GetCharacterMovement()->MaxWalkSpeed = WalkingSpeed;    
}

void AMyTheif::SetMovementSpeedAndAir()
{
	auto CharSpeed = GetVelocity();
	auto LateralSpeed = FVector(CharSpeed.X, CharSpeed.Y, 0.0f);

	MovementSpeed = LateralSpeed.Size();

	bInAir = GetMovementComponent()->IsFalling();
}
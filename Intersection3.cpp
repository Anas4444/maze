// Fill out your copyright notice in the Description page of Project Settings.


#include "Intersection3.h"
#include "TrafficLight.h"
#include <random>

// Sets default values
AIntersection3::AIntersection3()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AIntersection3::BeginPlay()
{
	Super::BeginPlay();
	createTrafficLight();
}

// Called every frame
void AIntersection3::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIntersection3::createTrafficLight()
{
	FVector v1 = FVector(GetActorLocation().X+460.f, GetActorLocation().Y+460.f, GetActorLocation().Z);
	FRotator r1 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw, GetActorRotation().Roll);

	FVector v2 = FVector(GetActorLocation().X+460.f, GetActorLocation().Y-460.f, GetActorLocation().Z);
	FRotator r2 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw-90.f, GetActorRotation().Roll);

	FVector v3 = FVector(GetActorLocation().X-460.f, GetActorLocation().Y+460.f, GetActorLocation().Z);
	FRotator r3 = FRotator(GetActorRotation().Pitch, GetActorRotation().Yaw+90.f, GetActorRotation().Roll);

	tl[0] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v1, &r1));
	tl[1] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v2, &r2));
	tl[2] = Cast<ATrafficLight>(GetWorld()->SpawnActor(TrafficLight, &v3, &r3));

	constexpr int DOUBLE_MIN = 0;
	constexpr int DOUBLE_MAX = 1;
	std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_real_distribution<double> distr(DOUBLE_MIN, DOUBLE_MAX);

	double r[3];
	int s[3] = {0, 0, 0};
	for (int i=0; i<3; i++) {
		r[i] = distr(eng);
	}
	double temp;
	for (int i=0; i<3; i++) {
		temp = r[i];
		for (int j = 0; j<3; j++) {
			if (temp >= r[j]) s[i]++;
		}
	}
	for (int i=0; i<3; i++) {
		tl[i]->intersection = 1;
		tl[i]->order = s[i];
	}
}
